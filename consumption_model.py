import numpy as np
from scipy.interpolate import interp1d
import scipy.io as sio
import math
import config

class Consumption:
    def __init__(self):
        self.Weng = self.LoadMat(config.motor_omega, 'Weng')
        self.Torque = self.LoadMat(config.motor_torque, 'Torque')
        self.Eta = self.LoadMat(config.motor_efficiency, 'Eta')

        self.e_aux = interp1d(config.t_amb_array, config.MEaux, kind='cubic')

    def LoadMat(self, _file_name, _str_data):
        mat = sio.loadmat(_file_name)
        struct = mat[_str_data]
        return struct

    def CalculatePowerConsumed(self, velocity, we, time, road_grade, rs_length):
        # Torque needed
        t_e = config.wheel_radius * (0.5 * config.rho * config.SCx * math.pow(velocity + config.v_headwind, 2)
                                     + config.M * config.g * config.Crr + config.M * config.a) / 2

        # Mechanical energy consumed
        e_mec = 1 / config.eta_g * (0.5 * config.rho * config.SCx * math.pow(velocity + config.v_headwind, 2)
                                    + config.M * config.g * road_grade
                                    + config.M * config.g * config.Crr + config.M * config.a) * velocity * time

        wi = np.argmin(abs(self.Weng[0, :] - we))

        ti = np.argmin(abs(self.Torque[:, 0] - t_e))

        return e_mec / self.Eta[ti][wi] * config.eta_b + self.e_aux(config.t_amb) / 100000 * rs_length




