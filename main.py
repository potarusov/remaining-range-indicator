import numpy as np
import math
import matplotlib.pyplot as plt
import config
from consumption_model import Consumption

class Main:
    def __init__(self):
        self.distances = []

        self.simulations = []

        self.consumption = Consumption()

    def PlotAuxConsumptionVsAmbTemperature(self):
        fig2 = plt.figure(2)
        x2 = np.linspace(config.t_min, config.t_max, 100)
        plt.plot(x2, self.consumption.e_aux(x2), label = 'cubic', color = 'r')
        plt.xlabel('Temperature')
        plt.ylabel('Heating/cooling energy needs')
        plt.title('Power Consumption vs Ambient Temperature')
        plt.legend()
        plt.show()

    def Find(self, where, what):
        for i in range(0, np.size(where)):
            if np.array_equal(where[i], np.asarray(what)):
                break
        return i

    def Simulate(self):
        fig1 = plt.figure(1, figsize=(10,8)) # default is (8,6)
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title('Remaining Range')

        # Road segment length
        rs_length = np.empty([np.size(config.graph_adj_matrix, 0), np.size(config.graph_adj_matrix, 1)])
        e_elec = np.empty([np.size(config.graph_adj_matrix, 0), np.size(config.graph_adj_matrix, 1)])

        # For each road segment calculate its entire cost in terms of energy
        # consumption taking into account the road segment grade and road segment legal speed
        for i in range(0, np.size(config.graph_adj_matrix, 0)):
            for j in range(0, np.size(config.graph_adj_matrix, 1)):
                if config.graph_adj_matrix[i][j]:
                    rs_length[i][j] = math.sqrt(math.pow(config.nodes[i][0] - config.nodes[j][0], 2) + math.pow(config.nodes[i][1] - config.nodes[j][1], 2))
                    velocity = min(config.v_driver, config.v_legal[i][j])
                    we = velocity / config.wheel_radius
                    time = rs_length[i][j] / velocity

                    # Electric power consumed
                    e_elec[i][j] = self.consumption.CalculatePowerConsumed(velocity, we, time, config.road_grade[i][j], rs_length[i][j])

                    if velocity == 16:
                        plt.plot([config.nodes[i][0], config.nodes[j][0]], [config.nodes[i][1], config.nodes[j][1]],
                                 color = 'r', linewidth = 3.0)

                    elif velocity == 22:
                        plt.plot([config.nodes[i][0], config.nodes[j][0]], [config.nodes[i][1], config.nodes[j][1]],
                                 color='b', linewidth=3.0)

                    elif velocity == 30:
                        plt.plot([config.nodes[i][0], config.nodes[j][0]], [config.nodes[i][1], config.nodes[j][1]],
                                 color='k', linewidth=3.0)


        plt.plot(config.nodes[:,0], config.nodes[:,1], 'bs')

        queue = []

        # Enqueue the first node - the initial vehicle location
        queue.append(config.nodes[0])

        # Array of examined nodes
        examined = np.zeros(np.size(config.graph_adj_matrix, 0))

        # Route graph node remaining range value(RRV)
        rem_range = np.zeros(np.size(config.graph_adj_matrix, 0))

        # Initial RRV for the initial vehicle location
        rem_range[0] = config.init_capacity

        while queue:
            v = queue[0]

            ind_in_nodes = self.Find(config.nodes, v)

            examined[ind_in_nodes] = 1
            queue.remove(v)

            for i in range(1, np.size(config.graph_adj_matrix, 0)):
                if (config.graph_adj_matrix[ind_in_nodes][i] and rem_range[ind_in_nodes] > 0):
                    ray_to_draw = min(e_elec[i][ind_in_nodes], rem_range[ind_in_nodes])

                    # then to draw the ray
                    cos_alpha = (config.nodes[i][0] - config.nodes[ind_in_nodes][0]) / e_elec[i][ind_in_nodes]
                    new_x = config.nodes[ind_in_nodes][0] + cos_alpha * ray_to_draw
                    sin_alpha = (config.nodes[i][1] - config.nodes[ind_in_nodes][1]) / e_elec[i][ind_in_nodes]
                    new_y = config.nodes[ind_in_nodes][1] + sin_alpha * ray_to_draw
                    plt.plot([new_x, config.nodes[ind_in_nodes][0]], [new_y, config.nodes[ind_in_nodes][1]],
                             color = 'g', linewidth=3.0)

                    if not examined[i]:
                        queue.append(config.nodes[i])
                        rem_range[i] = rem_range[ind_in_nodes] - ray_to_draw
                        examined[i] = 1

        plt.show()

main = Main()
main.Simulate()
main.PlotAuxConsumptionVsAmbTemperature()










