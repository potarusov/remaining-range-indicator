clear;

load Weng.mat;
load Torque.mat;
load eff.mat;

Vertices = [300 300; %1 - initial vehicle location
            290 305; %2
            300 310; %3
            310 305; %and so on
            310 300; 
            295 290;
            290 295;
            270 305;
            290 320;
            315 315;
            320 295;
            300 280;
            275 290;%13
            330 290];%14

Graph = [0 1 1 1 1 1 1 0 0 0 0 0 0 0;
         1 0 1 0 0 0 1 1 0 0 0 0 0 0;
         1 1 0 1 0 0 0 0 1 0 0 0 0 0;
         1 0 1 0 1 0 0 0 0 1 0 0 0 0;
         1 0 0 1 0 1 0 0 0 0 1 0 0 0;
         1 0 0 0 1 0 1 0 0 0 0 1 0 0;
         1 1 0 0 0 1 0 0 0 0 0 0 1 0;
         0 1 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 1 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 1 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 1 0 0 0 0 0 0 0 0 1;
         0 0 0 0 0 1 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 1 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 1 0 0 0];

%Road segment grade    
RSGrade = [ 0 1 1 1 1 1 1 0 0 0 0 0 0 0;
            1 0 1 0 0 0 1 1 0 0 0 0 0 0;
            1 1 0 1 0 0 0 0 1 0 0 0 0 0;
            1 0 1 0 1 0 0 0 0 1 0 0 0 0;
            1 0 0 1 0 1 0 0 0 0 1 0 0 0;
            1 0 0 0 1 0 1 0 0 0 0 1 0 0;
            1 1 0 0 0 1 0 0 0 0 0 0 1 0;
            0 1 0 0 0 0 0 0 0 0 0 0 0 0;
            0 0 1 0 0 0 0 0 0 0 0 0 0 0;
            0 0 0 1 0 0 0 0 0 0 0 0 0 0;
            0 0 0 0 1 0 0 0 0 0 0 0 0 1;
            0 0 0 0 0 1 0 0 0 0 0 0 0 0;
            0 0 0 0 0 0 1 0 0 0 0 0 0 0;
            0 0 0 0 0 0 0 0 0 0 1 0 0 0];

%Road segment legal speed        
Vleg = [    0 16 16 16 16 16 16 0 0 0 0 0 0 0;
            16 0 22 0 0 0 22 30 0 0 0 0 0 0;
            16 22 0 22 0 0 0 0 30 0 0 0 0 0;
            16 0 22 0 22 0 0 0 0 30 0 0 0 0;
            16 0 0 22 0 22 0 0 0 0 30 0 0 0;
            16 0 0 0 22 0 22 0 0 0 0 30 0 0;
            16 22 0 0 0 22 0 0 0 0 0 0 30 0;
            0 30 0 0 0 0 0 0 0 0 0 0 0 0;
            0 0 30 0 0 0 0 0 0 0 0 0 0 0;
            0 0 0 30 0 0 0 0 0 0 0 0 0 0;
            0 0 0 0 30 0 0 0 0 0 0 0 0 30;
            0 0 0 0 0 30 0 0 0 0 0 0 0 0;
            0 0 0 0 0 0 30 0 0 0 0 0 0 0;
            0 0 0 0 0 0 0 0 0 0 30 0 0 0];
        
%Charging stations' locations
ChStations = [1 1 0 0 1 0 0 0 1 0 1 0 0 0];                

Vertices = Vertices.*1000;      %Map scale
RSGrade = RSGrade.*0.0;
SoC_init = 0.1;                %Initial State-of-Charge
Cap_max=24700*3600;             %Battery capacity (J)
Cap_init=SoC_init*Cap_max;      %Initial battery capacity

%Parameters
Vdriver = 22;                   %Driver-desired speed
g = 9.81;
a = 0;                          %Acceleration
r = 0.3;                        %Wheel radius
M = 1700;                       %Vehicle mass
SCx = 0.7;
eta_g = 0.9;                    %Transmission efficiency
eta_b = 0.7;                    %Battery efficiency

%Variables
Crr = 0.012;                    %Rolling resistance
Vhw = 0;                        %Headwind speed
Tamb = 20;                     %Ambient temperature
rho = 1.2;                     %Air density: at 30� - 1.16, at 20� - 1.20, at 10� - 1.24, at 0� - 1.29, at -10 - 1.34, at -20� - 1.39

%Auxiliary energy needs
MTemperature = [30:-10:-20]; %from 30�C to -20�C
MEaux = [1000 0 3000 7000 8000 9500]; %[Wh] Mitsubishi i-MiEV energy needs for heating/cooling (100 km) measured at 30�C, 20�C, 10�C, 0�C, -10�C and -20�C (Dr. Werner Tober)

Eaux = interp1(MTemperature,MEaux,Tamb)*3600; %[J] A(or E) = P*t Derived heating/cooling energy needs

%Road segment length
RSLength = NaN*ones(size(Graph,1), size(Graph,2));
Eelec = NaN*ones(size(Graph,1), size(Graph,2));

hold on

%for each road segment calculate its entire cost in terms of energy
%consumption taking into account the road segment grade and road segment legal speed
for i=1:size(Graph,1)
    for j=1:size(Graph,2)
        if Graph(i,j)
            RSLength(i,j) = sqrt((Vertices(i,1) - Vertices(j,1))^2 + (Vertices(i,2) - Vertices(j,2))^2);
            Vel = min(Vdriver, Vleg(i,j));
            we = Vel/r;
            t = RSLength(i,j)/Vel;
            Te = r*(0.5*rho*SCx*(Vel+Vhw)^2 + M*g*Crr + M*a)/2; %Torque needed
            Emec=1/eta_g*(0.5*rho*SCx*(Vel+Vhw)^2 + M*g*RSGrade(i,j) + M*g*Crr + M*a)*Vel*t; %Mechanical energy consumed
            [mini wi]=min(abs(Weng(1,:)-we));
            [mini ti]=min(abs(Torque(:,1)-Te));
            Eelec(i,j) = Emec/Eta(ti,wi)*eta_b + Eaux/100000*RSLength(i,j);
            if Vel == 16 plot([Vertices(i,1) Vertices(j,1)], [Vertices(i,2) Vertices(j,2)],'Color','red','LineWidth',3);
            end
            if Vel == 22 plot([Vertices(i,1) Vertices(j,1)], [Vertices(i,2) Vertices(j,2)],'Color','blue','LineWidth',3);
            end
            if Vel == 30 plot([Vertices(i,1) Vertices(j,1)], [Vertices(i,2) Vertices(j,2)],'Color','black','LineWidth',3);
            end
        end
    end
end

plot(Vertices(:,1), Vertices(:,2),'o','LineWidth',3);

Queue = [];
Queue = [Queue Vertices(1,:)];          %Enqueue the first node - the initial vehicle location 
Examined = zeros(1,size(Graph,1));      %Array of examined nodes
RemRange = zeros(1,size(Graph,1));      %Route graph node remaining range value (RRV)
RemRange(1) = Cap_init;                 %Initial RRV for the initial vehicle location

while ~isempty(Queue)
    v = Queue(1,:);
    
    %IndInVertices - index of the node 'v' in 'Vertices' array
    IndInVertices = find((Vertices(:,1) == v(1) & Vertices(:,2) == v(2))==1); 
    Examined(IndInVertices) = 1;
    Queue(1,:) = [];
    
    for i=2:size(Graph,1)
        if Graph(IndInVertices,i) && RemRange(IndInVertices) > 0
            RayToDraw = min(Eelec(i,IndInVertices), RemRange(IndInVertices));
            
            %then to draw the ray
            CosAlpha = (Vertices(i,1) - Vertices(IndInVertices,1))/Eelec(i,IndInVertices);
            NewX = Vertices(IndInVertices,1) + CosAlpha*RayToDraw;
            SinAlpha = (Vertices(i,2) - Vertices(IndInVertices,2))/Eelec(i,IndInVertices);
            NewY = Vertices(IndInVertices,2) + SinAlpha*RayToDraw;
            plot([NewX Vertices(IndInVertices,1)], [NewY Vertices(IndInVertices,2)],'Color','green','LineWidth',3);   
            
            if(~Examined(i))
                Queue = [Queue; Vertices(i,:)];
                RemRange(i) = RemRange(IndInVertices) - RayToDraw;
                Examined(i) = 1;
            end
        end
    end
end

konec = 1;



