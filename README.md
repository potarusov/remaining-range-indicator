# Remaining Range Indicator for Electric Vehicles 
## (independent project with the financial support of GRETTIA, IFSTTAR)

Range estimation remains complicated because:

+ Future driving behavior is often unknown.

+ Road data as well as weather and traffic conditions (often uncertain) have to be taken into account.

+ Batteries are subject to external influences and have a limited lifespan.

Modern remaining range indicators are very inaccurate. They estimate the electric vehicle’s remaining range by means of a simple equation: Range = Battery State of Charge / Average Consumption per Mile at Average Velocity. The remaining range is displayed on the dashboard in the form of a circle, with the center at the vehicle’s current location and with the radius representing the vehicle’s distance to empty. Distance to empty is the actual distance the vehicle can be driven before recharging is required.

The idea of the project was to estimate the remaining range more accurately by taking into account the conditions mentioned above, and to present it as a remaining range graph that indeed provides a more detailed representation than a circle. The representation also provides a detailed illustration of the electric vehicle’s distance to empty.

To build a remaining range graph, I developed the Remaining Range Indicator System that couples the Breadth-First Search (BFS) algorithm with the electric energy consumption model of a vehicle [1-2].

## References

[1] Potarusov, R. and Lebacque, J.-P., “Breadth-First Search-Based Remaining Range Estimation for Electric Vehicle,” SAE Technical Paper 2014-01-0273, 2014, doi: 10.4271/2014-01-0273.

[2] Potarusov, R., Kobersy, I. and Lebacque, J.-P., “Remaining Range Indicator System for Electric Vehicle,” IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), 6th Workshop on Planning, Perception and Navigation for Intelligent Vehicles (PPNIV’14), Chicago, USA, September 14-18, 2014.