import numpy as np

# PARAMETERS

# Data files
motor_omega = "Weng.mat"
motor_torque = "Torque.mat"
motor_efficiency = "eff.mat"

# Driver-desired speed
v_driver = 22

# Acceleration
g = 9.81

# Wheel radius
wheel_radius = 0.3

# Vehicle mass
M = 1700

SCx = 0.7

# Rolling resistance
Crr = 0.012

# Transmission efficiency
eta_g = 0.9

# Battery
# Initial State-of-Charge
init_SoC = 0.25

# Battery capacity (J)
battery_capacity = 24700*3600

# Initial battery capacity
init_capacity = init_SoC*battery_capacity

# Battery efficiency
eta_b = 0.7


# VARIABLES
nodes = np.array([[300, 300], [290, 305], [300, 310], [310, 305], [310, 300], [295, 290], [290, 295], [270, 305],
                  [290, 320], [315, 315], [320, 295], [300, 280], [275, 290], [330, 290]])

# Graph Adjacency Matrix
graph_adj_matrix = np.array([[0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
                             [1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
                             [1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                             [1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                             [1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
                             [1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0],
                             [1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
                             [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                             [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]])

# Road segment grade
road_grade = np.array([[0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
                    [1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0],
                    [1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                    [1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                    [1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
                    [1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0],
                    [1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
                    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0]])

# Road segment legal speed
v_legal = np.array([[0, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0, 0, 0],
                    [16, 0, 22, 0, 0, 0, 22, 30, 0, 0, 0, 0, 0, 0],
                    [16, 22, 0, 22, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0],
                    [16, 0, 22, 0, 22, 0, 0, 0, 0, 30, 0, 0, 0, 0],
                    [16, 0, 0, 22, 0, 22, 0, 0, 0, 0, 30, 0, 0, 0],
                    [16, 0, 0, 0, 22, 0, 22, 0, 0, 0, 0, 30, 0, 0],
                    [16, 22, 0, 0, 0, 22, 0, 0, 0, 0, 0, 0, 30, 0],
                    [0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 30],
                    [0, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 0]])

# Charging stations' locations
charging_stations = np.array([1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0])

# Map scale
nodes = nodes*10000

road_grade = road_grade*0

a = 0

# Headwind speed
v_headwind = 0

# Ambient temperature
t_amb = 20

# Air density: at 30C - 1.16, at 20C - 1.20, at 10C - 1.24, at 0C - 1.29, at -10C - 1.34, at -20C - 1.39
rho = 1.2

# Auxiliary energy needs
# from 30C to -20C
t_min = -20
t_max = 30
t_amb_array = np.linspace(t_min, t_max, num=6, endpoint=True)

# [Wh] Mitsubishi i-MiEV energy needs for heating/cooling (100 km = 100000 m)
# measured at 30C, 20C, 10C, 0C, -10C and -20C (Dr. Werner Tober)
MEaux = np.array([9500.0, 8000.0, 7000.0, 3000.0, 0.0, 1000.0])

